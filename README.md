# NetAdmin Pull

Pull configuration to setup my Net Admin server

### KeepassXC Vault
We are now using KeepassXC to store secrets.
You will need to provision the Keepass Database ahead of time and store it in `$HOME/ansible.kdbx`.
Configure the password in Bitwarden.

### Pull this repo
`git clone https://gitlab.com/djarbz/ansible/netadmin-pull.git`

### Execute the Playbook
We will need to run the script using `bash` until I set the exec bit on the permissions and push to the repo.
`bash netadmin-pull/run.sh`

### Ansible Pull method (Non-functional with Galaxy Roles see [Issue/PR](https://github.com/ansible/ansible/pull/76549))
~~`~/.local/bin/ansible-pull --ask-become-pass --url https://gitlab.com/djarbz/ansible/netadmin-pull.git local.yaml`~~

### Future runs (Won't work due to bitwarden login being interactive)
~~`export BW_SESSION="$(bw unlock --raw)"`~~
~~`~/.local/bin/ansible-pull --only-if-changed --url https://gitlab.com/djarbz/ansible/netadmin-pull.git local.yaml`~~