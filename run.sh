#!/bin/bash
cd "$(dirname "$0")"

pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

set -e

# Generate SSH Client key if not found
if [ ! -f ~/.ssh/id_ed25519 ]
then
	echo "SSH Client key not found, generating a new one."
  ssh-keygen -t ed25519 -C "$(whoami)@$(hostname -d)"
fi

# Install pre-requisite packages
sudo apt update
sudo apt install -y \
  bind9-utils \
  curl \
  dbus \
  dnsutils \
  git \
  htop \
  inotify-tools \
  jq \
  python3 \
  python3-pip \
  sshpass \
  unzip \
  wireguard

# If Bitwarden is not installed, call the task to do so.
if [ ! -f $HOME/.local/bin/bw ]; then
  mkdir -p $HOME/.local/bin/
  wget -qO- 'https://vault.bitwarden.com/download/?app=cli&platform=linux' | busybox unzip - -od $HOME/.local/bin/
  chmod 700 $HOME/.local/bin/bw
fi

if [ -f $HOME/.env ]; then
  source $HOME/.env
fi

pathadd $HOME/.local/bin

# Login to Bitwarden if needed
case $(bw status | jq -r .status) in
  unauthenticated)
    export BW_SESSION=$(bw login --raw)
    ;;

  locked)
    export BW_SESSION=$(bw unlock --raw)
    ;;

  unlocked)
    echo "Bitwarden is unlocked!"
    ;;
  
  *)
    echo "Unknown Status"
    bw status | jq .status
    exit 1
    ;;
  
esac

# Save the session to be loaded on future runs.
echo "export BW_SESSION=${BW_SESSION}" > $HOME/.env

# Sync the Bitwarden Vault
bw sync

# Export the Root User password for the NetAdmin machine
export ANSIBLE_BECOME_PASS="$(bw get item "Ansible Vault" | jq -r '.fields[] | select(.name == "'$(hostname)'_root_pass") | .value')"


# Update the git repo and install/update any requirements.
git pull
ansible-galaxy install -f -r files/galaxy.requirements.yaml
pip install --user --upgrade -r files/requirements.txt

if [ -z "$ANSIBLE_BECOME_PASS" ]; then
  time $HOME/.local/bin/ansible-playbook --ask-become-pass local.yaml
else
  time $HOME/.local/bin/ansible-playbook local.yaml
fi
