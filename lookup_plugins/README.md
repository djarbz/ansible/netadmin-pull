## Enabling lookup plugins
Ansible enables all lookup plugins it can find. You can activate a custom lookup by either dropping it into a lookup_plugins directory adjacent to your play, inside the plugins/lookup/ directory of a collection you have installed, inside a standalone role, or in one of the lookup directory sources configured in ansible.cfg.

Default location is `$HOME/.ansible/plugins/lookup/bitwarden.py`

## Examples

### Get a single password after sync

```yaml
# Get password for Google
- debug:
    msg: {{ lookup('bitwarden', 'Google', sync=true) }}
```

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": "mysecret"
    }
```

### Get a single username

```yaml
# Get username for Google
- debug:
    msg: {{ lookup('bitwarden', 'Google', field='username') }}
```

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": "alice"
    }
```

### Get a single uri

```yaml
# Get uri for Google
- debug:
    msg: {{ lookup('bitwarden', 'Google', field='uri') }}
```

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": "google.com"
    }
```

### See all available fields

```yaml
# Get all available fields for an entry
- debug:
    msg: {{ lookup('bitwarden', 'Google', field='item') }}
```

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": {
        "favorite": false,
        "fields": [
            {
                "name": "mycustomfield",
                "type": 0,
                "value": "the value of my custom field"
            }
        ],
        "folderId": null,
        "id": "12345678-0123-4321-0000-a97001342c31",
        "login": {
            "password": "mysecret",
            "passwordRevisionDate": null,
            "totp": null,
            "username": "alice"
        },
        "name": "Google",
        "notes": null,
        "object": "item",
        "organizationId": "87654321-1234-9876-0000-a96800ed2b47",
        "revisionDate": "2018-10-19T19:20:17.923Z",
        "type": 1
    }
}
```

### Get the value of a custom field

```yaml
# Get the value of a custom field
- debug:
    msg: {{ lookup('bitwarden', 'Google', field='mycustomfield', custom_field=true) }}
```

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": "the value of my custom field"
    }
```

### download attachments files

```yaml
# Get the value of a custom field
- debug:
    msg: {{ lookup('bitwarden', 'privateKey.pem',  itemid='123456-1234-1234-abbf-60c345aaa3e', attachments=true ) }}
```
Optional parameters - output='/ansible/publicKey.pem'

The above might result in:

```
TASK [debug] *********************************************************
ok: [localhost] => {
    "msg": "Saved /publicKey.pem"
    }
```
