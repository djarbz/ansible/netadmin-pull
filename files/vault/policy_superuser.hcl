# Allow an admin all permissions on the vault instance
path "*" {
  capabilities = ["create", "read", "update", "patch", "delete", "list"]
}
