# Grant Read and Update permissions on the ansible KV secret engine.
# This should allow ansible to read secrets and update them as needed.
path "ansible/*" {
  capabilities = ["create", "read", "update"]
}
path "gen/password" {
  capabilities = ["create", "update"]
}
path "gen/passphrase" {
  capabilities = ["create", "update"]
}
