---
- name: Set Service Name Fact
  ansible.builtin.set_fact:
    service_name: dns

# Setup Technitium DNS container
- ansible.builtin.include_role: 
    name: notmycloud.podman_systemd
  vars:
    PODMAN_SYSTEMD_DEPLOY:
      ansible:
        systemd:
          containers:
            technitium-dns:
              podman_options:
                image: "docker.io/technitium/dns-server:latest"
                network: "host" # Enable this when serving DHCP
                # ports:
                  # - "5380:5380/tcp" # HTTP Web Interface
                  # - "53443:53443/tcp" # HTTPS Web Interface
                  # - "153:53/tcp" # DNS TCP Port
                  # - "153:53/udp" # DNS UDP Port
                  # - "67:67/udp" # DHCP service
                  # - "853:853/tcp" # DNS-over-TLS service
                  # - "443:443/tcp" # DNS-over-HTTPS service
                  # - "80:80/tcp" # DNS-over-HTTPS service certbot certificate renewal
                  # - "8053:8053/tcp" # DNS-over-HTTPS using reverse proxy
                volumes: 
                  - host: /etc/localtime
                    container: /etc/localtime
                    options: ro
                  - host: $XDG_CONFIG_HOME/technitium-dns
                    container: /etc/dns/config
                    options: rw
                    create:
                      state: directory
                      mode: u=rwx,g=rx,o=
                # environment:
                  # DNS_SERVER_DOMAIN: {{ net_base_domain }} # The primary domain name used by this DNS Server to identify itself.
                  # DNS_SERVER_ADMIN_PASSWORD: password # DNS web console admin user password.
                  # DNS_SERVER_ADMIN_PASSWORD_FIL: =password.txt # The path to a file that contains a plain text password for the DNS web console admin user.
                  # DNS_SERVER_PREFER_IPV6: false # DNS Server will use IPv6 for querying whenever possible with this option enabled.
                  # DNS_SERVER_OPTIONAL_PROTOCOL_DNS_OVER_HTTP: false # Enables DNS server optional protocol DNS-over-HTTP on TCP port 8053 to be used with a TLS terminating reverse proxy like nginx.
                  # DNS_SERVER_RECURSION: UseSpecifiedNetworks # Recursion options Allow, Deny, AllowOnlyForPrivateNetworks, UseSpecifiedNetworks.
                  # DNS_SERVER_RECURSION_DENIED_NETWORKS: 1.1.1.0/24 # Comma separated list of IP addresses or network addresses to deny recursion. Valid only for `UseSpecifiedNetworks` recursion option.
                  # DNS_SERVER_RECURSION_ALLOWED_NETWORKS: 127.0.0.0/8,192.168.0.0/16,172.16.0.0/12,10.0.0.0/8,100.64.0.0/10 # Comma separated list of IP addresses or network addresses to allow recursion. Valid only for `UseSpecifiedNetworks` recursion option.
                  # DNS_SERVER_ENABLE_BLOCKING: true # Sets the DNS server to block domain names using Blocked Zone and Block List Zone.
                  # DNS_SERVER_ALLOW_TXT_BLOCKING_REPORT: true # Specifies if the DNS Server should respond with TXT records containing a blocked domain report for TXT type requests.
                  # DNS_SERVER_FORWARDERS: 1.1.1.1,1.0.0.1,8.8.8.8,8.8.4.4 # Comma separated list of forwarder addresses.
                  # DNS_SERVER_FORWARDER_PROTOCOL: Tcp # Forwarder protocol options: Udp, Tcp, Tls, Https, HttpsJson.
                  # DNS_SERVER_LOG_USING_LOCAL_TIME: true # Enable this option to use local time instead of UTC for logging.
                labels:
                  diun.enable: true
                other_options:
                  # - '--init'
                  - '--cap-drop=all'
                  - '--security-opt=no-new-privileges'
                  - '--userns keep-id'

- ansible.builtin.include_tasks:  tasks/traefik-dynamic-config.yaml
  vars:
    task_prefix: "{{ service_name }}"
    traefik_file_name: "{{ service_name }}.{{ net_base_domain }}.yaml"
    traefik_endpoint_server: "http://{{ net_host_ip }}:{{ port_local_dns_http }}/"
    # traefik_access_level: public
    traefik_service_name: "{{ service_name }}"
    hostnames:
      - record: "{{ service_name }}"
      - record: "{{ service_name }}-home"
      - record: "{{ service_name }}-chuwi"

# - name: Create Cloudflare CNAME Record(s) for Technitium DNS Server
#   community.general.cloudflare_dns:
#     api_token: "{{ CLOUDFLARE_API_TOKEN }}"
#     zone: "{{ net_base_domain }}"
#     record: "{{ dns.record }}"
#     type: "{{ dns.type | default('CNAME') }}"
#     value: "{{ dns.value | default('traefik.home.'+net_base_domain) }}"
#     proxied: "{{ dns.proxied | default('true') }}"
#     solo: true # Delete all other records with the same record & type.
#     state: present
#   loop_control:
#     loop_var: dns
#   loop:
#     - record: "{{ service_name }}"
#     - record: "{{ service_name }}.home"
#       proxied: false

# # Setup Traefik configuration
# - name: Traefik | Pre-Configuration
#   block:
#   - name: Traefik | Create configuration directory
#     ansible.builtin.file:
#       path: "$XDG_CONFIG_HOME/traefik/conf.d"
#       state: directory
#       mode: '0750'
#       recurse: true

#   - name: Traefik | Create endpoint configuration
#     ansible.builtin.copy:
#       dest: "$XDG_CONFIG_HOME/traefik/conf.d/{{ endpoint.filename }}"
#       content: "{{ endpoint.content }}"
#       mode: '0600'
#     when: (endpoint.enable is not defined) or (endpoint.enable is defined and endpoint.enable)
#     loop: "{{ traefik.file_config }}"
#     loop_control:
#       loop_var: endpoint
#     # TODO: Only enable configuration if hostname is publicly resolvable

#   - name: Traefik | Delete disabled endpoints
#     ansible.builtin.file:
#       path: "$XDG_CONFIG_HOME/traefik/conf.d/{{ endpoint.filename }}"
#       state: absent
#     when: (endpoint.enable is defined and not endpoint.enable)
#     loop: "{{ traefik.file_config }}"
#     loop_control:
#       loop_var: endpoint

#   vars:
#     traefik:
#       file_config:
#         - filename: "{{ service_name }}.{{ net_base_domain }}.yaml"
#           hostnames:
#             - "{{ service_name }}.{{ net_base_domain }}"
#             - "{{ service_name }}.home.{{ net_base_domain }}"
#           enable: true
#           content: |
#             # HTTPS Proxy for {{ service_name }}.{{ net_base_domain }}
#             http:
#               routers:
#                 {{ service_name }}-local:
#                   rule: "(Host(`{{ service_name }}.{{ net_base_domain }}`) || Host(`{{ service_name }}.home.{{ net_base_domain }}`)) && ClientIP(`10.0.0.0/8`, `100.64.0.0/10`, `172.16.0.0/12`, `192.168.0.0/16`)"
#                   tls:
#                     certResolver: letsEncrypt
#                   service: {{ service_name }}
#                 {{ service_name }}-remote:
#                   rule: "Host(`{{ service_name }}.{{ net_base_domain }}`) || Host(`{{ service_name }}.home.{{ net_base_domain }}`)"
#                   middlewares:
#                     - chain-private-oauth
#                   tls:
#                     certResolver: letsEncrypt
#                   service: {{ service_name }}
#               services:
#                 {{ service_name }}:
#                   loadBalancer:
#                     servers:
#                       - url: "http://{{ net_dns_ip }}:{{ port_local_dns_http }}/"
