# Linux Capabilities

In the Systemd configuration for a service it is best practice to disable unneeded capabilities.
Below is a snippet for a basic set of options that can be expanded upon as needed.
```yaml
other_options:
    - '--cap-drop=all'
    - '--security-opt=no-new-privileges'
    - '--userns keep-id'
```


The full list of available Linux capabilities for the active kernel can be displayed using the capsh command.
These may differ from system to system.
```bash
$ sudo capsh --print
    Current: =ep
    Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read,cap_perfmon,cap_bpf,cap_checkpoint_restore
    Ambient set =
    Current IAB:
    Securebits: 00/0x0/1'b0
    secure-noroot: no (unlocked)
    secure-no-suid-fixup: no (unlocked)
    secure-keep-caps: no (unlocked)
    secure-no-ambient-raise: no (unlocked)
    uid=0(root) euid=0(root)
    gid=0(root)
    groups=0(root)
    Guessed mode: UNCERTAIN (0)
```


For the capabilities your instance of Podman has run info
```bash
$ podman info | grep capabilities
```

## Capability Notes

These notes are a WIP as I discover options that are needed for certain scenarios.  
More information about capabilities can be found here: https://man7.org/linux/man-pages/man7/capabilities.7.html

### Special Notes

For `Sudo`, do not use the `--security-opt=no-new-privileges` option.
Requires capabilities `setuid` & `setgid`.

AUDIT_WRITE
- Write records to kernel auditing log.

CHOWN
- Make arbitrary changes to file UIDs and GIDs.
- Used to allow the `chown` command.

DAC_OVERRIDE
- Bypass file read, write, and execute permission checks.

FOWNER
- Bypass permission checks on operations that normally require the
  filesystem UID of the process to match the UID of the file
  (e.g., chmod(2), utime(2)), excluding those operations covered
  by DAC_OVERRIDE and DAC_READ_SEARCH.
- set inode flags (see ioctl_iflags(2)) on arbitrary files.
- set Access Control Lists (ACLs) on arbitrary files.
- ignore directory sticky bit on file deletion.
- modify user extended attributes on sticky directory owned by any user.
- specify O_NOATIME for arbitrary files in open(2) and fcntl(2).
- Used to allow the `chmod` command.

FSETID
- Don't clear set-user-ID and set-group-ID mode bits when a file is modified.
- set the set-group-ID bit for a file whose GID does not match the filesystem
  or any of the supplementary GIDs of the calling process.
  
KILL
- Bypass permission checks for sending signals (see kill(2)).
  This includes use of the ioctl(2) KDSIGACCEPT operation.

MKNOD
- Create special files using mknod(2).

NET_BIND_SERVICE
- Bind a socket to Internet domain privileged ports (port numbers less than 1024).

NET_RAW
- Use RAW and PACKET sockets.
- bind to any address for transparent proxying.

SETFCAP
- Set arbitrary capabilities on a file.
- Since Linux 5.12, this capability is also needed to map user ID 0
  in a new user namespace; see user_namespaces(7) for details.

SETGID
- Make arbitrary manipulations of process GIDs and supplementary GID list.
- forge GID when passing socket credentials via UNIX domain sockets.
- write a group ID mapping in a user namespace (see user_namespaces(7)).
- required for sudo

SETPCAP
- Add any capability from the calling thread's bounding set to its
  inheritable set; drop capabilities from the bounding set 
  (via prctl(2) PR_CAPBSET_DROP); make changes to the securebits flags.

SETUID
- Make arbitrary manipulations of process UIDs
  (setuid(2), setreuid(2), setresuid(2), setfsuid(2)).
- forge UID when passing socket credentials via UNIX domain sockets.
- write a user ID mapping in a user namespace (see user_namespaces(7)).
- required for sudo

SYS_CHROOT
- Use chroot(2).
- change mount namespaces using setns(2).



# All Capabilites
chown
dac_override
dac_read_search
fowner
fsetid
kill
setgid
setuid
setpcap
linux_immutable
net_bind_service
net_broadcast
net_admin
net_raw
ipc_lock
ipc_owner
sys_module
sys_rawio
sys_chroot
sys_ptrace
sys_pacct,
sys_admin
sys_boot
sys_nice
sys_resource
sys_time
sys_tty_config
mknod
lease
audit_write
audit_control
setfcap
mac_override
mac_admin
syslog
wake_alarm
block_suspend
audit_read
perfmon
bpf
checkpoint_restore
